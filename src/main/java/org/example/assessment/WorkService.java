package org.example.assessment;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.assessment.model.Author;
import org.example.assessment.model.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

//search for works by author on Authors API side
@Service
public class WorkService
{
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private AuthorService authorService;

    private String API_URL = "https://openlibrary.org/authors/";
    public String getWorks(String authorName) {
        String authorKey = null;
        try
        {
            authorKey = authorService.getAuthorKey(authorName);
            String url = API_URL + authorKey + "/works.json";
            return restTemplate.getForObject(url, String.class);
        } catch (Exception e)
        {
            e.printStackTrace();
            return "error";
        }
    }
    //function to extract all works from result when searching for works on Authors API side
    public Iterable<Work> parseWorksFromJson(String jsonResponse, Author author) throws Exception {
        System.out.println(jsonResponse);
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(jsonResponse);
        JsonNode entriesNode = rootNode.path("entries");

        ArrayList<Work> works = new ArrayList<>();

        if (entriesNode.isArray() && (!entriesNode.isEmpty())) {
            for (JsonNode entryNode : entriesNode) {
                Work work = new Work();
                if (entryNode.has("title")) {
                    work.setName(entryNode.path("title").asText());
                }
                if (entryNode.has("created")) {
                    String createdDateString = entryNode.path("created").path("value").asText();
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
                    work.setDate(LocalDate.parse(createdDateString, formatter));
                }
                work.setAuthor(author);
                works.add(work);
            }
        }

        return works;
    }
}
