package org.example.assessment;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.assessment.model.Author;
import org.example.assessment.repos.AuthorRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import java.util.Optional;

@RestController
public class AuthorController {

    @Autowired
    private AuthorService authorService;
    @Autowired
    private AuthorRepo authorRepo;
    @Autowired
    private ObjectMapper objectMapper;
    //get all authors stored in the database
    @GetMapping("/getAllAuthors")
    public @ResponseBody Iterable<Author> getAllEmployees()
    {
        return authorRepo.findAll();
    }
    //get author by name
    @GetMapping("/getAuthor/{name}")
    public String getAuthor(@PathVariable(name = "name") String name) {
        Optional<Author> author = authorRepo.findByName(name);
        try {
            //if author is not present in database, search is performed at OpenLibrary
            if (!author.isPresent()) {
                author = Optional.ofNullable(authorService.parseAuthorFromJson(authorService.getAuthors(name)));
                //if found at OpenLibrary, author is saved to database
                authorRepo.save(author.get());
            }
            return objectMapper.writeValueAsString(author.get());
        } catch (Exception e) {
            //As OpenLibrary has it own search algorithms,
            //to prevent duplicates, Author name column is unique only,
            //if duplicate causes exception, correct name is being extracted
            //and used to perform search again
            //Can't say I am proud of this...
            String message = e.getMessage();
            if ((message != null) && message.contains("Duplicate entry")) {
                int startIndex = message.indexOf("'");
                int endIndex = message.indexOf("' for key");
                if (((startIndex != -1) && (endIndex != -1)) && (startIndex < endIndex)) {
                     return getAuthor(message.substring(startIndex + 1, endIndex));
                }
            }
            e.printStackTrace();
            return "error";
        }
    }

    //adds new author manually through JSON body
    @PostMapping("/addAuthor")
    public Author addAuthor(@RequestBody Author author)
    {
        return authorRepo.save(author);
    }

    //deletes author from the database by id
    @DeleteMapping(value = "/deleteAuthor/{id}")
    public @ResponseBody Optional<Author> deleteAuthor(@PathVariable(name = "id") int id){
        authorRepo.deleteById(id);
        return authorRepo.findById(id);
    }


}
