package org.example.assessment.repos;

import org.example.assessment.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorRepo extends JpaRepository<Author, Integer>
{
    Optional<Author> findByName(String name);
}
