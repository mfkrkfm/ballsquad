package org.example.assessment.repos;

import org.example.assessment.model.Work;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkRepo extends JpaRepository<Work, Integer>
{
    Iterable<Work> findByAuthorId(int authorId);
}
