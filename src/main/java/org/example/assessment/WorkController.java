package org.example.assessment;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.assessment.model.Author;
import org.example.assessment.model.Work;
import org.example.assessment.repos.AuthorRepo;
import org.example.assessment.repos.WorkRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;

@RestController
public class WorkController
{
    @Autowired
    private WorkRepo workRepo;

    @Autowired
    private AuthorRepo authorRepo;

    @Autowired
    private WorkService workService;

    @Autowired
    private ObjectMapper objectMapper;

    //adds work manually through JSON body
    @PostMapping("/addWork")
    public Work addWork(@RequestBody Work work)
    {
        return workRepo.save(work);
    }
    //get all works stored in the database
    @GetMapping("/getAllWorks")
    public @ResponseBody Iterable<Work> getAllWorks()
    {
        return workRepo.findAll();
    }
    //search for works by author id, seach is performed in the database
    //if not found, search is continued at Open Library
    @GetMapping("/getWorksByAuthor/{id}")
    public @ResponseBody Iterable<Work> getWorksByAuthor(@PathVariable(name = "id") int id) {
        Iterable<Work> work = workRepo.findByAuthorId(id);
        try {
            if ((work != null) && !work.iterator().hasNext()) {
                Author author = authorRepo.findById(id).get();
                work = workService.parseWorksFromJson(workService.getWorks(author.getName()), author);
                workRepo.saveAll(work);
            }
            return work;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    //deletes work from the database by id
    @DeleteMapping(value = "/deleteWork/{id}")
    public @ResponseBody Optional<Work> deleteWork(@PathVariable(name = "id") int id) {
        workRepo.deleteById(id);
        return workRepo.findById(id);
    }
}
