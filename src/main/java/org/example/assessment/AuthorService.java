package org.example.assessment;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.assessment.model.Author;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

//search for author on Authors API side
@Service
public class AuthorService {

    @Autowired
    private RestTemplate restTemplate;

    private static final String API_URL = "https://openlibrary.org/search/authors.json?q=";

    public String getAuthors(String authorName) throws Exception {
        String url = API_URL + authorName;
        return restTemplate.getForObject(url, String.class);
    }

    //function retrieves Authors API author key, to perform search for his works
    //other way is to store keys in the database
    public String getAuthorKey(String authorName) throws Exception {

        String jsonResponse = getAuthors(authorName);
        System.out.println("JSON Response: " + jsonResponse);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(jsonResponse);
        JsonNode docsNode = rootNode.path("docs");
        if (docsNode.isArray() && docsNode.size() > 0) {
            JsonNode firstDocNode = docsNode.get(0);
            return firstDocNode.path("key").asText();
        }
        return null;
    }

    //function to extract first author from result when searching for author on Authors API side
    public Author parseAuthorFromJson(String jsonResponse) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(jsonResponse);
        JsonNode docsNode = rootNode.path("docs");

        if (docsNode.isArray() && (!docsNode.isEmpty())) {
            JsonNode firstDocNode = docsNode.get(0);

            Author author = new Author();
            author.setName(firstDocNode.path("name").asText());

            if (firstDocNode.has("birth_date")) {
                String birthDateString = firstDocNode.path("birth_date").asText();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMMM yyyy");
                author.setBirth_date(LocalDate.parse(birthDateString, formatter));
            }
            author.setTop_work(firstDocNode.path("top_work").asText());
            author.setWork_count(firstDocNode.path("work_count").asInt());

            return author;
        }

        return null;
    }
}

