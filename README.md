# API LIBRARY SERVICE

### Requirements:
1. MySQL database at `localhost:3306` with root password: `toor`
2. Free 8080 port
3. Any API client (Postman)
4. JRE >= 8.0
5. JDK >= 21
6. MAVEN
### How to build:
1. Download source code
2. `cd` into downloaded source code folder in terminal
3. compile using `mvn compile`
4. create jar using `mvn package`
5. run `java -jar target/assessment-0.0.1-SNAPSHOT.jar`
6. Testing could be performed using any API testing tool (Postman)

### List of available endpoints:
From the tasks:
1. Search authors by name:\
    GET `/getAuthor/{name}`
2. Search author's works by author id:\
   GET `/getWorksByAuthor/{id}`
Additional:
3. Get all authors stored in the database:\
   GET `/getAllAuthors`
4. Add new author manually through JSON body:\
    POST `/addAuthor`
5. Delete author from the database by id:\
    DELETE `/deleteAuthor/{id}`
6. Add work manually through JSON body:\
    POST `/addWork`
7. Get all works stored in the database:\
    GET `/getAllWorks`
8. Delete work from the database by id:\
    DELETE `/deleteWork/{id}`

### Example for testing:

1. GET `http://localhost:8080/getAuthor/Mark%20Twain`
2. GET `http://localhost:8080/getAuthor/tolkien`
3. GET `http://localhost:8080/getWorksByAuthor/1`
4. GET `http://localhost:8080/getWorksByAuthor/2`
